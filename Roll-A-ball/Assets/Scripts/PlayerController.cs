﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System.Threading;
using System.Security.Cryptography;
using System.Diagnostics;


public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;
    public Transform player;
    public Transform respawnPoint;
    public float timeRemaining = 60;
    public GameObject timeoutTextObject;


    Vector3 lastVelocity;
   


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winTextObject.SetActive(false);
        timeoutTextObject.SetActive(false);
    }
    void Update()
    {
        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
        }
        else
        {
            UnityEngine.Debug.Log("Time has run out!");
            player.gameObject.SetActive(false);
            timeoutTextObject.SetActive(true);
        }
    }
    void OnMove(InputValue movementValue)
    {
 
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
        

    }
    void SetCountText()
    {
        countText.text = "Score: " + count.ToString();
        if (count == 1000)
        {
            winTextObject.SetActive(true);
        }
    }
    void FixedUpdate()
    {

        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);
        transform.Rotate(new Vector3(0, 180, 0) * Time.deltaTime);
        lastVelocity = rb.velocity;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            other.gameObject.SetActive(false);
            count += 25;

            SetCountText();
        }
        /*if (other.gameObject.CompareTag("BadPickup"))
        {
            other.gameObject.SetActive(false);
            count -= 10;

            SetCountText();
        }*/
        if (other.gameObject.CompareTag("Respawn"))
        {
            /*code from https://www.youtube.com/watch?v=nBgCeJBMT0k
              with some modifications*/
            player.transform.position = respawnPoint.transform.position;
            rb.velocity = rb.velocity *= 0.5f;
        }
    }
    void OnCollisionEnter(Collision coll)
    {
        /*code from https://www.youtube.com/watch?v=RoZG5RARGF0
         */

            var speed = lastVelocity.magnitude;
        var direction = Vector3.Reflect(lastVelocity.normalized, coll.contacts[0].normal);
        rb.velocity = direction * Mathf.Max(speed, 1f);
        rb.velocity = rb.velocity *= 1.004f;
    }
}

